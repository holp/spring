package edu.msu.commandline.config;


// http://www.baeldung.com/spring-data-jpa-multiple-databases

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.boot.context.properties.ConfigurationProperties;
import javax.persistence.EntityManagerFactory;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;

//import com.google.common.base.Preconditions;

@Configuration
@PropertySource({ "classpath:test.properties" })
//@PropertySource({ "classpath:persistence-multiple-db.properties" })
//@PropertySource({ "classpath:application.properties" })
@EnableJpaRepositories(
		       basePackages = "edu.msu.commandline.repository.test", 
		       entityManagerFactoryRef = "testEntityManager", 
		       transactionManagerRef = "testTransactionManager"
)
public class TestConfig {

    
    // https://medium.com/@joeclever/using-multiple-datasources-with-spring-boot-and-spring-data-6430b00c02e7
    // https://github.com/jahe/spring-boot-multiple-datasources/blob/master/src/main/java/com/foobar/BarDbConfig.java
    @Bean(name = "testDataSource")
    @ConfigurationProperties(prefix = "test.datasource")
    public DataSource dataSource() {
	return DataSourceBuilder.create().build();
    }

    @Bean(name = "testEntityManager")
    public LocalContainerEntityManagerFactoryBean testEntityManager(
								    EntityManagerFactoryBuilder builder, @Qualifier("testDataSource") DataSource dataSource) {
	return builder.dataSource(dataSource).packages("edu.msu.commandline.entity.test").persistenceUnit("test").build();
    }
    
    @Bean(name = "testTransactionManager")
    public PlatformTransactionManager testTransactionManager(
							     @Qualifier("testEntityManager") EntityManagerFactory testEntityManager) {
	return new JpaTransactionManager(testEntityManager);
    }



    /*
    @Autowired
    private Environment env;

    @Bean
    //@Primary
    public LocalContainerEntityManagerFactoryBean testEntityManager() {
	
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(testDataSource());
        //em.setPackagesToScan(new String[] { "org.baeldung.persistence.multiple.model.user" });
        em.setPackagesToScan(new String[] { "edu.msu.commandline.entity.test" });
 
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);
	
        return em;
    }
    
    //@Primary
    @Bean
    public DataSource testDataSource() {

        //DriverManagerDataSource dataSource = new DriverManagerDataSource();
        //dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        //dataSource.setUrl(env.getProperty("user.jdbc.url"));
        //dataSource.setUsername(env.getProperty("jdbc.user"));
        //dataSource.setPassword(env.getProperty("jdbc.pass"));
	

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&amp;useJDBCCompliantTimezoneS‌​hift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC");
        dataSource.setUsername("test");
        dataSource.setPassword("test");

	// spring.datasource.url=jdbc:mysql://localhost:3306/test?useUnicode=true&amp;useJDBCCompliantTimezoneS‌​hift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC
	// spring.datasource.username=test
	// spring.datasource.password=test
	// spring.datasource.driver-class-name=com.mysql.jdbc.Driver
	
        return dataSource;
    }


    //@Primary
    @Bean
    public PlatformTransactionManager testTransactionManager() {
  
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(testEntityManager().getObject());
        return transactionManager;
    }
    */
}
