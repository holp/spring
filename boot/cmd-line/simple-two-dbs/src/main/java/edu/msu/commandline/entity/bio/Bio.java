package edu.msu.commandline.entity.bio;

// $Id: Bio.java,v 1.1 2018-02-22 04:08:52-04 ericholp Exp ericholp $

// bio table entity

import javax.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;

// Caused by: com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException: Table 'test.bio' doesn't exist

@Data
@Entity
@Table(name = "BIO")
public class Bio {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String biokey;
    private String sname;

    private String sid;

    //@Getter(AccessLevel.NONE) private String sid;

    /*
    public void setSid(String sid){
	this.sid=sid + "    ";
    }

    public String getSid(){
	return sid + "    ";
    }
    */
    
    /*
    public String getSid(){
	return "A12";
    }
    
    public void setSid(String sid){
	//this.sid=sid.trim();
	this.sid="A12";
    }

    public String getSname(){
	return "A12";
    }
    
    public void setSname(String sname){
	//this.sid=sid.trim();
	this.sname="A12";
    }
    */
    
    /*
    @Getter(AccessLevel.NONE) @Setter(AccessLevel.NONE) private String sid;

    public String getSid(){
	return sid.trim();
    }

    public void setSid(String sid){
	this.sid=sid.trim();
    }
    */
    
    /*
    @PostLoad
    protected void repair(){
        if(sid != null){
	    sid = sid.trim();
	}
    }

    
    public String getSid(){
	return sid.trim();
    }

    public void setSid(String sid){
	this.sid=sid.trim();
    }
    */
}
