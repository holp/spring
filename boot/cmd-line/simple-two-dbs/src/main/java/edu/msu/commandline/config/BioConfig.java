package edu.msu.commandline.config;


// http://www.baeldung.com/spring-data-jpa-multiple-databases

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.boot.context.properties.ConfigurationProperties;
import javax.persistence.EntityManagerFactory;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;

//import com.google.common.base.Preconditions;

@Configuration
@PropertySource({ "classpath:bio.properties" })
//@PropertySource({ "classpath:persistence-multiple-db.properties" })
//@PropertySource({ "classpath:application.properties" })
@EnableJpaRepositories(
		       basePackages = "edu.msu.commandline.repository.bio", 
		       entityManagerFactoryRef = "bioEntityManager", 
		       transactionManagerRef = "bioTransactionManager"
)
public class BioConfig {

    /*
    @Autowired
    //@Qualifier("sfadb")
    public DataSource dataSource;
    */


    // https://medium.com/@joeclever/using-multiple-datasources-with-spring-boot-and-spring-data-6430b00c02e7
    // https://github.com/jahe/spring-boot-multiple-datasources/blob/master/src/main/java/com/foobar/BarDbConfig.java
    @Primary
    @Bean(name = "bioDataSource")
    @ConfigurationProperties(prefix = "bio.datasource")
    public DataSource dataSource() {
	return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "bioEntityManager")
    public LocalContainerEntityManagerFactoryBean bioEntityManager(
								   EntityManagerFactoryBuilder builder, @Qualifier("bioDataSource") DataSource dataSource) {
	return builder.dataSource(dataSource).packages("edu.msu.commandline.entity.bio").persistenceUnit("bio").build();
    }
    
    @Primary
    @Bean(name = "bioTransactionManager")
    public PlatformTransactionManager bioTransactionManager(
							    @Qualifier("bioEntityManager") EntityManagerFactory bioEntityManager) {
	return new JpaTransactionManager(bioEntityManager);
    }


    /*
    @Autowired
    private Environment env;

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean bioEntityManager() {
	
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(bioDataSource());
        //em.setPackagesToScan(new String[] { "org.baeldung.persistence.multiple.model.user" });
        em.setPackagesToScan(new String[] { "edu.msu.commandline.entity.bio" });
 
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);
	
        return em;
    }
    
    @Primary
    @Bean
    public DataSource bioDataSource() {

        //DriverManagerDataSource dataSource = new DriverManagerDataSource();
        //dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        //dataSource.setUrl(env.getProperty("user.jdbc.url"));
        //dataSource.setUsername(env.getProperty("jdbc.user"));
        //dataSource.setPassword(env.getProperty("jdbc.pass"));
	

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        dataSource.setUrl("jdbc:oracle:thin:@SFAQA190D.itservices.msu.edu:1521/sfaqa");
        dataSource.setUsername("J5600_BATCH_DB");
        dataSource.setPassword("J5600$Batch$Db");
	
	//datasource.sfadb.driverClassName=oracle.jdbc.driver.OracleDriver
	//datasource.sfadb.url=jdbc:oracle:thin:@SFAQA190D.itservices.msu.edu:1521/sfaqa
	//datasource.sfadb.portNumber=1521
	//datasource.sfadb.databaseName=sfaqa
	//datasource.sfadb.testOnBorrow=true
	//datasource.sfadb.validationQuery=select 1 from dual
	//datasource.sfadb.username=J5600_BATCH_DB
	//datasource.sfadb.password=J5600$Batch$Db

        return dataSource;
    }


    @Primary
    @Bean
    public PlatformTransactionManager bioTransactionManager() {
  
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(bioEntityManager().getObject());
        return transactionManager;
    }
    */
}
