package edu.msu.commandline.app;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


import edu.msu.commandline.entity.test.Test;
import edu.msu.commandline.repository.test.TestRepository;
import edu.msu.commandline.entity.bio.Bio;
import edu.msu.commandline.repository.bio.BioRepository;

import edu.msu.commandline.config.BioConfig;
import edu.msu.commandline.config.TestConfig;

// https://github.com/eugenp/tutorials/blob/master/persistence-modules/spring-jpa/src/test/java/org/baeldung/persistence/service/JpaMultipleDBIntegrationTest.java
//import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


//import java.util.List;
//import java.util.ArrayList;
//import java.util.Arrays;

/**
 * @author ERH
 */
//@Configuration
//@Profile("test")
@Component
//@ContextConfiguration(classes = { BioConfig.class, TestConfig.class })
public class App {
    //@SuppressWarnings("unused")
    //private static final Logger log = LoggerFactory.getLogger(App.class.getName());

    @Value("${var}")
    private String var;

    //@Autowired
    //public DataSource dataSource;

    @Autowired
    private TestRepository testrepo;

    @Autowired
    private BioRepository biorepo;    


    /*
mysql> select * from test;
+----+--------------+
| id | field        |
+----+--------------+
|  9 | a test       |
| 11 | nibley blip  |
| 14 | kibley snard |
| 15 | an insert    |
| 16 | mooby        |
| 17 | kkkk         |
| 18 | m            |
| 19 | jjjj         |
+----+--------------+
8 rows in set (0.00 sec)
    */


    // SELECT SNAME FROM BIO WHERE TRIM(SID) = 'A58034512';

    @Transactional("testTransactionManager")
    public void testtest(){
	//System.out.println("var=" + var);

	//System.out.println();
	//System.out.println("datasource=" + dataSource);

	//System.out.println();
	//System.out.println("repo=" + repo);
	
	Test test = testrepo.findById(14);
	System.out.println("field for id=14 => " + test.getField());
    }

    @Transactional("bioTransactionManager")
    public void testbio(){
	//System.out.println("var=" + var);

	//System.out.println();
	//System.out.println("datasource=" + dataSource);

	//System.out.println();
	//System.out.println("repo=" + repo);
	
	//Test test = testrepo.findById(14);
	//System.out.println("field for id=14 => " + test.getField());


	if(biorepo==null){
	    System.out.println("biorepo null");
	}else{
	    // https://stackoverflow.com/questions/5725491/trim-string-field-in-jpa
	    
	    //Bio bio1 = biorepo.findBySid("A58034512");
	    Bio bio1 = biorepo.findBySid("A58034512    ");
	    if(bio1==null){
		System.out.println("bio is null");
	    }else{
		System.out.println("SNAME for SID=A58034512 => " + bio1.getSname());
	    }
	    
	    // slow because of trim... probably better to simply pass in SID w/ added 4 spaces
	    
	    List<Bio> bios = biorepo.findBySidTrimmed("A58034512");
	    if(bios==null){
		System.out.println("no bio hits for SID=A58034512");
	    }else{
		for(Bio bio : bios){
		    System.out.println("SNAME for SID=A58034512 => " + bio.getSname());
		}
	    }

	}

    }
}
