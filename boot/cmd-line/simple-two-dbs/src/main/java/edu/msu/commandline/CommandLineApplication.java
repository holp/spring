package edu.msu.commandline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import edu.msu.commandline.app.App;

@SpringBootApplication
public class CommandLineApplication implements CommandLineRunner {

    @Autowired
    private App app;

    @Override
    public void run(String... args) {
	app.testtest();
	app.testbio();
 	
	if (args.length > 0 && args[0].equals("exitcode")) {
	    throw new ExitException();
	}
    }
    
    public static void main(String[] args) {
	SpringApplication.run(CommandLineApplication.class, args);
    }
}
