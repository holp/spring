package edu.msu.commandline.repository.bio;

// $Id: BioRepository.java,v 1.1 2018-02-21 16:52:40-04 ericholp Exp ericholp $

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import edu.msu.commandline.entity.bio.Bio;

import java.util.List;

public interface BioRepository extends JpaRepository<Bio, Integer>{
    public Bio findBySid(String sid);
    
    @Query(value = "SELECT b FROM Bio b where trim(sid)=:sid")
    List<Bio> findBySidTrimmed(@Param("sid") String sid);
}
