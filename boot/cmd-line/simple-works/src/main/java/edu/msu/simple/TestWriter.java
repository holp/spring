package edu.msu.simple;


//import edu.msu.simple.ProSAMWriterException;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

import edu.msu.simple.model.TestModel;
import edu.msu.simple.model.TestFileOutputModel;

/**
 * ERH
 */
public class TestWriter implements ItemWriter<TestModel>, ItemStream {

    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    
    protected BufferedWriter writer;

    private String filepref;
    
    @Value("${output-dir}")
    private String outputdir;

    public BufferedWriter createWriter() {

        File outFile = new File(outputdir + "/output");
	
        try {
            outFile.createNewFile();   
        } catch (IOException ex) {
            throw new RuntimeException("Error file could not be created.", ex);
        }

        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("File encoding unsupported.", ex);
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Error file could not be found.", ex);
        }

        return writer;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        createWriter();
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
    }

    @Override
    public void close() throws ItemStreamException {
        try {
            this.writer.close();
	} catch (IOException ex) {
            log.error("Unable to close writer.", ex);
            try {
                this.writer.flush();
            } catch (IOException ex2) {
                log.error("Unable to flush writer.", ex2);
            }
        }
    }
    
    @Override
    public void write(List<? extends TestModel> models) {
	for(TestModel model : models) {
	    try {
		this.writer.write(TestFileOutputModel.builder().model(model).build().getRecords());
	    }catch(IOException ex) {
		System.out.println("There was a problem writing the model " + model + ex);
		//throw new ProSAMWriterException("There was a problem writing the model " + model, ex);
	    }
	}
    }
}
