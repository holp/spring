package edu.msu.simple;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import edu.msu.simple.model.TestModel;

import org.springframework.batch.core.launch.support.RunIdIncrementer;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;

import javax.annotation.PostConstruct;


///
import org.springframework.batch.item.file.transform.Range;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
///

import org.springframework.batch.item.file.transform.FixedLengthTokenizer;


@Configuration
//@Profile("test")
@EnableBatchProcessing
public class TestBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestBatch.class.getName());

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @PostConstruct
    public void init() {
	System.out.println("batch");
    }


    //@StepScope
    @Bean
    public TestReader reader() {
        return new TestReader();
    }

    /*
    @Bean
    public ItemReader<TestModel> reader() {
        FlatFileItemReader<TestModel> reader = new FlatFileItemReader<TestModel>();
        reader.setResource(new ClassPathResource("input-files/test-input-file"));
        reader.setLineMapper(new DefaultLineMapper<TestModel>() {{
            setLineTokenizer(new DelimitedLineTokenizer() {{
                setNames(new String[] {"value"});
		Range[] ranges = new Range[1];
		ranges[0] = new Range(1, 3);
		//setColumns(ranges);
            }});
            //setFieldSetMapper(new TestFieldSetMapper<TestModel>() {{
	    setFieldSetMapper(new TestFieldSetMapper() {{
                //setTargetType(TestModel.class);
            }});
        }});
        return reader;
    }
    */


    /*
    @Bean
    public ItemReader<TestModel> reader() {
        FlatFileItemReader<TestModel> reader = new FlatFileItemReader<TestModel>();
        reader.setResource(new ClassPathResource("input-files/test-input-file"));
        reader.setLineMapper(new DefaultLineMapper<TestModel>() {{
            setLineTokenizer(new FixedLengthTokenizer() {{
                setNames(new String[] {"value"});
		Range[] ranges = new Range[1];
		ranges[0] = new Range(1, 3);
		setColumns(ranges);
            }});
            //setFieldSetMapper(new TestFieldSetMapper<TestModel>() {{
	    setFieldSetMapper(new TestFieldSetMapper() {{
                //setTargetType(TestModel.class);
            }});
        }});
        return reader;
    }
    */
    
    @Bean
    public TestProcessor processor() {
        return new TestProcessor();
    }

    /*
    @Bean
    public TestWriter writer() {
        return new TestWriter();
    }
    */    

    @Bean
    public Step testStep() {
        return stepBuilderFactory.get("testStep").<TestModel, TestModel>chunk(10)
	    .reader(reader())
	    .processor(processor())
	    //.writer(writer())
	    .build();
    }
    
    @Bean
    public Job testLoad() throws Exception {
	System.out.println("Loading...");
        return jobBuilderFactory
	    .get("testLoad")
	    .incrementer(new RunIdIncrementer())
	    .start(testStep())
	    .build();
    }
}
