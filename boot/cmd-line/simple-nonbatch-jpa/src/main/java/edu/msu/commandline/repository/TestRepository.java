package edu.msu.commandline.repository;

// $Id: TestRepository.java,v 1.5 2018-02-21 16:24:15-04 ericholp Exp $

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;

import edu.msu.commandline.entity.Test;

//import java.util.List;

public interface TestRepository extends JpaRepository<Test, Integer>{
    public Test findById(int id);
    //@Query(value = "SELECT f FROM RsaFileDataForm f JOIN f.rsasForm r where r.id=:rsaid and f.name=:filename order by f.name")
    //List<RsaFileDataForm> findBasedOnIdAndFilename(@Param("rsaid") int rsaid, @Param("filename") String filename);
}
