package edu.msu.commandline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import edu.msu.commandline.app.App;


/*
Caused by: edu.msu.commandline.ExitException: null

java -jar build/libs/command-line-0.0.1-SNAPSHOT.jar exitcode
    
Erics-MacBook-Pro:command-line ericholp$ echo $?
10
*/

@SpringBootApplication
public class CommandLineApplication implements CommandLineRunner {

    @Autowired
    private App app;

    @Override
    public void run(String... args) {
	app.test();

	if(args.length > 0){
	    System.out.println("arg[0]=" + args[0]);
	}
	
	if (args.length > 0 && args[0].equals("exitcode")) {
	    throw new ExitException(2);
	}
    }
    
    public static void main(String[] args) {
	SpringApplication.run(CommandLineApplication.class, args);
    }
}
