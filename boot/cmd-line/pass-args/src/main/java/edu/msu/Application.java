package edu.msu;

// $Id: Application.java,v 1.10 2018-07-26 13:23:27-04 ericholp Exp $

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.beans.factory.annotation.Value;

// https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-spring-application.html
import org.springframework.boot.ApplicationArguments;
import java.util.List;
import java.util.Set;

// run without a datasource defined
// https://stackoverflow.com/questions/32074631/spring-boot-application-without-a-datasource
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
public class Application implements CommandLineRunner {

    // https://memorynotfound.com/spring-boot-passing-command-line-arguments-example/
    // below specified as "--correspondenceType=<correspondenceType>"... if not specified jar will exception out immediately
    @Value("${correspondenceType}")
    private String correspondenceType;

    @Autowired
    private ApplicationArguments argsinjected;

    @Override
    public void run(String... args) {

	int i=0;
	for(String arg : args){
	    System.out.println("args[" + i + "]=" + arg);
	    i++;
	}

	// run with --correspondenceType=<value>
	System.out.println("correspondenceType=" + correspondenceType);
	
	String[] a = argsinjected.getSourceArgs();
	
	boolean debug = argsinjected.containsOption("debug");
	List<String> files = argsinjected.getNonOptionArgs();
	// if run with "--debug logfile.txt" debug=true, files=["logfile.txt"] << actually, no

	Set<String> optnames = argsinjected.getOptionNames();
	for(String optname : optnames){
	    List<String> opts = argsinjected.getOptionValues(optname);
	    for(String opt : opts){
		System.out.println(optname + " " + opt);
	    }
	}
	

    }
    
    public static void main(String[] args) {
	SpringApplication.run(Application.class, args);
    }
}
