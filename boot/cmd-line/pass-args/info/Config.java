package edu.msu;

import org.springframework.boot.context.properties.ConfigurationProperties;

//import org.springframework.batch.core.Job;
//import org.springframework.batch.core.Step;
//import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
//import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
//import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Calendar;

import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.annotation.PostConstruct;

//import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
//import org.springframework.batch.core.launch.support.RunIdIncrementer;

//import org.springframework.batch.item.ItemReader;

//import org.springframework.boot.ApplicationArguments;
import java.util.List;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;

import java.sql.SQLException;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableAutoConfiguration
//@EnableBatchProcessing
public class Config extends DefaultBatchConfigurer {

    @Override
    public void setDataSource(DataSource dataSource) {
        // override to do not set datasource even if a datasource exist.
        // initialize will use a Map based JobRepository (instead of database)
    }

    /*
    @Bean(name="filetracking")
    @ConfigurationProperties(prefix="datasource.filetracking")
    public DataSource restSource() throws SQLException {
        return DataSourceBuilder.create().build();
    }
    */
    
}
