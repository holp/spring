package edu.msu.listeners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/*
@SpringBootApplication
public class ListenersApplication {
    public static void main(String[] args) {
	SpringApplication.run(ListenersApplication.class, args);
    }
}
*/


import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import org.springframework.context.ConfigurableApplicationContext;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;


@SpringBootApplication
@EnableBatchProcessing
public class ListenersApplication {
    
    public static void main(String[] args) {
	ListenersApplication obj = new ListenersApplication();
	obj.run();
    }
    
    private void run() {
	//String[] springConfig = { "spring/batch/jobs/job-read-files.xml" };
	//ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);
	
	String[] args = {};
	
	ConfigurableApplicationContext context = SpringApplication.run(ListenersApplication.class, args);
	//Job j = ctx.getBean(Job.class);
	//JobLauncher jl = ctx.getBean(JobLauncher.class);
	//JobParameters param = new JobParametersBuilder().addLong("run.id", System.currentTimeMillis()).toJobParameters();
	//JobExecution execution = jl.run(j, param);

	//JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
	JobLauncher jobLauncher = (JobLauncher) context.getBean(JobLauncher.class);

	/*
	Job job = (Job) context.getBean("readMultiFileJob");
	
	try {
	    JobExecution execution = jobLauncher.run(job, new JobParameters());
	    System.out.println("Exit Status : " + execution.getStatus());
	    System.out.println("Exit Status : " + execution.getAllFailureExceptions());
	    
	} catch (Exception e) {
	    e.printStackTrace();
	}
	*/
	
	System.out.println("Done");
    }
}
