package edu.msu.simple;

import lombok.Builder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestFileOutputModel {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());
    
    private TestModel model;

    @Builder
    public TestFileOutputModel(TestModel model) {
        this.model = model;
    }

    public String getRecords() {
	String s="";

	if(model.getValue() == null){
	    s+="\"\"";
	}else{
	    s+="\"" + model.getValue() + "\"";
	}

        StringBuilder record = new StringBuilder(s.length());
        //record.append(StringUtils.leftPad("", s.length()));
        record.append(s);
        record.append("\n");

        return record.toString();
    }
}
