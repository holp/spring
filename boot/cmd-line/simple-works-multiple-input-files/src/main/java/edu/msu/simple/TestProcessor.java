package edu.msu.simple;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import edu.msu.simple.TestModel;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.compress.utils.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class TestProcessor implements ItemProcessor<TestModel, TestModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestProcessor.class.getName());

    // these are only read from aplication.properties
    @Value("${input-dir}")
    private String inputdir;
    
    @Value("${output-dir}")
    private String outputdir;


    // https://memorynotfound.com/java-tar-example-compress-decompress-tar-tar-gz-files/
    public void compress(String name, File... files) throws IOException {
        try (TarArchiveOutputStream out = getTarArchiveOutputStream(name)){
            for (File file : files){
                addToArchiveCompression(out, file, ".");
            }
        }
    }

    public void compress2(String name, List<File> files) throws IOException {
        try (TarArchiveOutputStream out = getTarArchiveOutputStream(name)){
	    
            for (File file : files){
                addToArchiveCompression(out, file, ".");
            }
        }
    }
    
    public void decompress(String in, File out) throws IOException {
        try (TarArchiveInputStream fin = new TarArchiveInputStream(new FileInputStream(in))){
            TarArchiveEntry entry;
            while ((entry = fin.getNextTarEntry()) != null) {
                if (entry.isDirectory()) {
                    continue;
                }
                File curfile = new File(out, entry.getName());
                File parent = curfile.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                IOUtils.copy(fin, new FileOutputStream(curfile));
            }
        }
    }

    
    private TarArchiveOutputStream getTarArchiveOutputStream(String name) throws IOException {
        TarArchiveOutputStream taos = new TarArchiveOutputStream(new GzipCompressorOutputStream(new FileOutputStream(name)));
        taos.setBigNumberMode(TarArchiveOutputStream.BIGNUMBER_STAR);
        taos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
        taos.setAddPaxHeadersForNonAsciiNames(true);
        return taos;
    }

    private void addToArchiveCompression(TarArchiveOutputStream out, File file, String dir) throws IOException {
        String entry = dir + File.separator + file.getName();
        if (file.isFile()){
            out.putArchiveEntry(new TarArchiveEntry(file, entry));
            try (FileInputStream in = new FileInputStream(file)){
                IOUtils.copy(in, out);
            }
            out.closeArchiveEntry();
        } else if (file.isDirectory()) {
            File[] children = file.listFiles();
            if (children != null){
                for (File child : children){
                    addToArchiveCompression(out, child, entry);
                }
            }
        } else {
            System.out.println(file.getName() + " is not supported");
        }
    }


    
    @Override
    public TestModel process(final TestModel test) {
	System.out.println("in processor: test.getValue()=" + test.getValue());
	/*
	System.out.println(test.getValue());

	List<File> files  = new ArrayList<File>();
	files.add(new File(inputdir + "/input-files/test-input-1"));
	
	files.add(new File(inputdir + "/input-files/test-input-2"));

	files.add(new File(inputdir + "/input-files/test-input-3"));

	try {
	    compress2(outputdir + "/out2.tgz", files);	    
	}catch(IOException ex){
	    System.out.println(ex);
	}
	*/
        return test;
    }

}
