package edu.msu.simple;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SimpleApplication {

    public static void main(String... args) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {
	ConfigurableApplicationContext ctx = SpringApplication.run(SimpleApplication.class, args);
        Job j = ctx.getBean(Job.class);
        JobLauncher jl = ctx.getBean(JobLauncher.class);
        JobParameters param = new JobParametersBuilder().toJobParameters();
        JobExecution execution = jl.run(j, param);
    }
}
