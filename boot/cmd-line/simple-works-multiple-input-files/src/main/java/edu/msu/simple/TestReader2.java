package edu.msu.simple;

//import edu.msu.j5600.CorrespondenceModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;

import org.springframework.batch.item.file.MultiResourceItemReader;

import org.springframework.core.io.ResourceLoader; 
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer; 
import org.springframework.batch.item.file.mapping.FieldSetMapper; 
import org.springframework.batch.item.file.mapping.PassThroughFieldSetMapper; 
import org.springframework.core.io.Resource; 
import org.springframework.context.ResourceLoaderAware; 


import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;

/**
 * @author ERH
 */
public class TestReader2 extends MultiResourceItemReader<TestModel> implements ResourceLoaderAware {
    
    private static final Logger log = LoggerFactory.getLogger(TestReader2.class.getName());

    private ResourceLoader resourceLoader;

    @Value("${input-dir}")
    private String inputdir;

    @Override 
    public void setResourceLoader(ResourceLoader resourceLoader) {
	this.resourceLoader = resourceLoader; 
    }

    @PostConstruct
    public void updateResourceAfterBeanInit() {

	ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(resourceLoader);

	String filefilter = "test-input-*";
	//filefilter = "msu_ltr_send_awd_*" + AIDYR2 + "_" + env + "_*";
	
	// msu_ltr_send_awd_notice19_dev__20180113_035129_UNX_30.csv
	// msu_ltr_send_fup19_dev__20180113_041448_UNX_50.csv

	System.out.println("looking for files matching: " + inputdir + "/" + filefilter);
	
	Resource[] resources = null;
	try {
	    resources = resolver.getResources("file:" + inputdir + "/" + filefilter);
	}catch(IOException ex){
	    log.error("updateResourceAfterBeanInit: getResources failed: " + ex);
	}

	System.out.println("number of matching files=" + resources.length);

	this.setResources(resources);

	FlatFileItemReader fileReader = new FlatFileItemReader();
	
	DefaultLineMapper<TestModel> lineMapper = new DefaultLineMapper<>();

	FixedLengthTokenizer tokenizer = new FixedLengthTokenizer();
        String[] names = {"value"};
        Range[] ranges = new Range[1];
        ranges[0] = new Range(1, 3);
        //ranges[0] = new Range(1, 1);
        tokenizer.setNames(names);
        tokenizer.setColumns(ranges);


	FieldSetMapper fieldSetMapper = new PassThroughFieldSetMapper();
	
	lineMapper.setLineTokenizer(tokenizer);
	lineMapper.setFieldSetMapper(fieldSetMapper);
        lineMapper.setFieldSetMapper(new TestFieldSetMapper());
	
	fileReader.setLineMapper(lineMapper);
	this.setDelegate(fileReader);

        this.open(new ExecutionContext());
    }
}
