package edu.msu.simple;

import edu.msu.simple.TestModel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;

/**
 * Created by yangji27 on 7/17/17. (Stolen by ERH.)
 */
public class TestFieldSetMapper implements FieldSetMapper<TestModel> {
    public Logger LOGGER = LoggerFactory.getLogger(getClass().getName());

    /*
    TestFieldSetMapper(){
	System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> in constructor");
    }
    */

    
    public TestModel mapFieldSet(FieldSet fieldSet) {
	//System.out.println(">>>");

        TestModel TestModel = new TestModel();
	
        if (StringUtils.isNotEmpty(fieldSet.readString(0))) {
	    //LOGGER.info("Value=" + fieldSet.readString(0));
	    System.out.println("in Field Set Mapper: Value=" + fieldSet.readString(0));
            TestModel.setValue(fieldSet.readString(0));
        }
        return TestModel;
    }
}
