package edu.msu.simple;


//import edu.msu.simple.configure.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Bean;

import org.springframework.batch.item.file.MultiResourceItemReader;

import org.springframework.core.io.ResourceLoader; 
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer; 
import org.springframework.batch.item.file.mapping.FieldSetMapper; 
import org.springframework.batch.item.file.mapping.PassThroughFieldSetMapper; 
import org.springframework.core.io.Resource; 
import org.springframework.context.ResourceLoaderAware; 


import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.FixedLengthTokenizer;
import org.springframework.batch.item.file.transform.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;


import java.io.IOException;

import edu.msu.simple.TestModel;


public class TestReader extends FlatFileItemReader<TestModel> {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestReader.class.getName());

    private ResourceLoader resourceLoader;
    
    private Resource resource;

    @Value("${input-dir}")
    private String inputdir;

    @Value("${output-dir}")
    private String outputdir;
    
    //@Autowired
    //private Config config;

    @PostConstruct
    public void updateResourceAfterBeanInit() {

	//System.out.println("running TestReader");
	
	String inputPath = inputdir + "/test-input-file";
	//String inputPath = inputdir + "/f0.job";
	//String inputPath = "/Users/ericholp/code/spring/boot/cmd-line/collect-file-info/dira/dirb/f0.job";

	System.out.println("in reader: inputPath=" + inputPath);

	FileSystemResource fr = new FileSystemResource(inputPath);
	
        //this.setResource(new FileSystemResource(inputPath));
        this.setResource(fr);
        DefaultLineMapper<TestModel> lineMapper = new DefaultLineMapper<TestModel>();

	try {
	    // -rw-r--r--  1 ericholp staff   7 12-19 15:39 test-input-file
	    System.out.println("in reader: contentLength=" + fr.contentLength()); // returns 7
	    System.out.println("in reader: getFilename=" + fr.getFilename()); // getFilename=test-input-file
	    //System.out.println("isReadable=" + fr.isReadable()); //
	}catch(IOException ex){
	    System.out.println(ex);
	}

	/*
	System.out.println("item cnt=" + this.getCurrentItemCount());
	
	if(this.getCurrentItemCount() == 0){
	    System.out.println("no items found");
	}
	*/
	
        FixedLengthTokenizer fileTokenizer = new FixedLengthTokenizer();
        String[] names = {"value"};
        Range[] ranges = new Range[1];
        ranges[0] = new Range(1, 3);
        //ranges[0] = new Range(1, 1);
        fileTokenizer.setNames(names);
        fileTokenizer.setColumns(ranges);

	try {
	    lineMapper.setLineTokenizer(fileTokenizer);
	    lineMapper.setFieldSetMapper(new TestFieldSetMapper());
	    this.setLineMapper(lineMapper);
	}catch(Exception ex){
	    System.out.println(ex);
	}

	ExecutionContext ec = new ExecutionContext();

	//System.out.println("isEmpty=" + ec.isEmpty());
	//System.out.println("size=" + ec.size());

	
	//this.open(new ExecutionContext());
	//this.open(ec);
	try {
	    this.doOpen();
	}catch(Exception ex){
	    System.out.println(ex);
	}
    }
}
