package edu.msu.simple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.batch.core.launch.support.RunIdIncrementer;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;


@Configuration
@EnableBatchProcessing
public class TestBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestBatch.class.getName());

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    /*
    @Bean
    public TestReader reader() {
        return new TestReader2();
    }
    */

    @Bean
    public TestReader2 reader() {
        return new TestReader2();
    }
    
    @Bean
    public TestProcessor processor() {
        return new TestProcessor();
    }

    @Bean
    public Step testStep() {
        return stepBuilderFactory.get("testStep").<TestModel, TestModel>chunk(10)
	    .reader(reader())
	    .processor(processor())
	    //.writer(writer())
	    .build();
    }
    
    @Bean
    public Job testLoad() throws Exception {
        return jobBuilderFactory
	    .get("testLoad")
	    .incrementer(new RunIdIncrementer())
	    .start(testStep())
	    .build();
    }
}
