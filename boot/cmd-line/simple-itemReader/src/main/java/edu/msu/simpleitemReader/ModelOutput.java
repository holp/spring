package edu.msu.simpleitemReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Builder;

public class ModelOutput {
	
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    private Model model;
    
    @Builder
    public ModelOutput(Model model) {
	this.model = model;
    }
    
    public String getRecords() {
	StringBuilder record = new StringBuilder(100);
	record.append(model.getValue0());
	record.append(" ");
	record.append(model.getValue1());
	record.append(" ");
	record.append(model.getValue2());
	record.append("\n");
	//log.info("workStudyRecord: " + workStudyRecord);
	return record.toString();
    }
}
