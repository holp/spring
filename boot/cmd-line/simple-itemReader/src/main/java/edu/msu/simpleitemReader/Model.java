package edu.msu.simpleitemReader;

import lombok.Data;

@Data
public class Model {	
    private String value0;
    private String value1;
    private String value2;
}
