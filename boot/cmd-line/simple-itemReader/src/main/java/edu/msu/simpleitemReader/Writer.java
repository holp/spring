package edu.msu.simpleitemReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class Writer implements ItemWriter<Model>, ItemStream {
    
    private static final Logger log = LoggerFactory.getLogger(Writer.class.getName());
    
    @Value("${output-file-location}")
    protected String outputFileLocation;
    
    //@Autowired
    //private BatchConfig config;
    
    protected BufferedWriter writer;
    
    public BufferedWriter createWriter() {
        //File outFile = new File(outputFileLocation + outputFileName);
        File outFile = new File("outfile");
	
        try {
            outFile.createNewFile();
        } catch (IOException ex) {
            throw new RuntimeException("Error file could not be created.", ex);
        }
	
        try {
	    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("File encoding unsupported.", ex);
        } catch (FileNotFoundException ex) {
            throw new RuntimeException("Error file could not be found.", ex);
        }
	
        return writer;
    }
    
    
    @Override
    public void write(List<? extends Model> models) throws Exception {
        for(Model model : models) {
            try {
                this.writer.write(ModelOutput.builder().model(model).build().getRecords());
            }catch(IOException ex) {
		System.out.println(ex);
                //throw new ProSAMWriterException("There was a problem writing Work Study records to file." +
		//				workStudyInput, ex);
            }
        }
    }
    
    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
	createWriter();
    }
    
    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {	}
    
    @Override
    public void close() throws ItemStreamException {
        try {
            this.writer.close();
        } catch (IOException ex) {
	    log.error("Unable to close Work Study writer.", ex);
            try {
                this.writer.flush();
            } catch (IOException ie) {
            	log.error("Unable to flush Work Study writer.", ie);
            }
        }
    }
}
