package edu.msu.simpleitemReader;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Calendar;

import javax.sql.DataSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.jdbc.core.JdbcTemplate;
import javax.annotation.PostConstruct;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.support.RunIdIncrementer;

import org.springframework.batch.item.ItemReader;

import org.springframework.boot.ApplicationArguments;
import java.util.List;

@Configuration
//@Profile("sdr")
@EnableBatchProcessing
public class Batch {

    private static final Logger log = LoggerFactory.getLogger(Batch.class.getName());

    @Autowired
    private ApplicationArguments args;
    
    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    /*
    @Value("${sdr-delete-table-values-first}")
    private boolean deletetablevalues;
    */


    @PostConstruct
    public void init() {
	boolean testdebug = args.containsOption("testdebug");
	List<String> files = args.getNonOptionArgs();
	// if run with "--debug logfile.txt" debug=true, files=["logfile.txt"] << actually, no

	System.out.println("testdebug=" + testdebug);
	
	for(String file : files){
	    System.out.println("file=" + file);
	}
    }

    // doesn't seem to run with the below... or only ran once..?
    //@StepScope
    @Bean
    public Reader reader() {
        return new Reader();
    }

    @Bean
    public Processor processor() {
        return new Processor();
    }

    @Bean
    public Writer writer() {
        return new Writer();
    }
    
    @Bean
    public Step Step() {
        return stepBuilderFactory.get("Step").<Model, Model>chunk(10)
	    .reader(reader())
	    .processor(processor())
	    .writer(writer())
	    .build();
    }

    @Bean
    public Job Job () throws Exception {
	return jobBuilderFactory.get("Job")
	    // had to add the below otherwise read in reader only ran once
	    // https://stackoverflow.com/questions/30165680/spring-batch-item-reader-is-executing-only-once
	    .incrementer(new RunIdIncrementer())
	    .start(Step())
	    .build();
    }
}
