package edu.msu.simpleitemReader;

//import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.jdbc.core.BeanPropertyRowMapper;
//import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
//import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.annotation.PostConstruct;

import java.util.ArrayList;
import java.util.List;


public class Reader implements ItemReader<Model> {

    
    private static final Logger log = LoggerFactory.getLogger(Reader.class.getName());

    private List<Model> models  = new ArrayList<Model>();
    private int index=0;
    
    @PostConstruct
    public void updateAfterBeanInit() {
	Model m0 = new Model();
        m0.setValue0("00");
        m0.setValue1("01");
        m0.setValue2("02");
	models.add(m0);

	Model m1 = new Model();
        m1.setValue0("10");
        m1.setValue1("11");
        m1.setValue2("12");
	models.add(m1);
 
	Model m2 = new Model();
        m2.setValue0("20");
        m2.setValue1("21");
        m2.setValue2("22");
	models.add(m2);
    }

    @Override
    public Model read() throws Exception {
        Model next = null;
	
	if(index < models.size()){
            next = models.get(index++);
        }
	
        return next;
    }
}
