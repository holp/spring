package edu.msu.simpleitemReader;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleItemReaderApplication {
    
    public static void main(String[] args) {
	SpringApplication.run(SimpleItemReaderApplication.class, args);
    }
}
