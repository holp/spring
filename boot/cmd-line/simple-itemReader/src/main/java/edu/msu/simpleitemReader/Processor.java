package edu.msu.simpleitemReader;

import org.springframework.batch.item.ItemProcessor;

import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;
import org.springframework.boot.ApplicationArguments;
import java.util.List;

public class Processor implements ItemProcessor<Model, Model>{

    @Autowired
    private ApplicationArguments args;

    @PostConstruct
    public void init() {
	boolean testdebug = args.containsOption("testdebug");
	List<String> files = args.getNonOptionArgs();

	System.out.println("processor testdebug=" + testdebug);
	
	for(String file : files){
	    System.out.println("processor file=" + file);
	}
    }
    
    @Override
    public Model process(Model model) throws Exception {
	
	return model;
    }
}
