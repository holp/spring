#!/bin/bash

if [[ -f outfile ]]; then
    rm outfile
fi

# java -jar build/libs/simple-itemReader-0.0.1.jar files=[file1, file2], debug=true
# java -jar build/libs/simple-itemReader-0.0.1.jar "--debug logfile.txt" debug=true, files=["logfile.txt"]
java -jar build/libs/simple-itemReader-0.0.1.jar --testdebug=true file1 file2

if [[ -f outfile ]]; then
    cat outfile
else
    echo outfile does not exist
fi
