package edu.msu.simple;

/*
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleApplication.class, args);
	}
}
*/

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;

//import edu.msu.simple.configure.Config;

/**
 * To build, use command
 *  SPRING_PROFILES_ACTIVE=local ./gradlew clean build
 * <p>
 * To run jar file, use command
 * java -jar -Dspring.profiles.active=local,[jobname] build/libs/J5600-service.jar
 * </p>
 * <p>
 * Note: -Dspring.profiles.active=dev defines system property. Here, different property files
 * will be read based on in put. Value of spring.profiles.active and me dev, test, QA and prod.
 * </p>
 */

@SpringBootApplication
//@EnableBatchProcessing
//@EnableConfigurationProperties(Config.class)
public class SimpleApplication {
    

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleApplication.class.getName());

    /*
    public static void main(String[] args) {
	SpringApplication.run(SimpleApplication.class, args);
    }
    */

    public static void main(String... args) throws JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException, JobParametersInvalidException {

	ConfigurableApplicationContext ctx = SpringApplication.run(SimpleApplication.class, args);
        Job j = ctx.getBean(Job.class);
        JobLauncher jl = ctx.getBean(JobLauncher.class);
        //JobParameters param = new JobParametersBuilder().addLong("run.id", System.currentTimeMillis()).toJobParameters();
        JobParameters param = new JobParametersBuilder().toJobParameters();
        JobExecution execution = jl.run(j, param);
	
        //int status = execution.getExitStatus().equals(ExitStatus.COMPLETED) ? 0 : -1;
        //LOGGER.info("Exit Status : " + status);
        //System.exit(status);
    }

}
