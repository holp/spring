package edu.msu.simple.test;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import edu.msu.simple.test.model.TestModel;

import org.springframework.batch.core.launch.support.RunIdIncrementer;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;

@Configuration
//@Profile("test")
@EnableBatchProcessing
public class TestBatch {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestBatch.class.getName());

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    //@StepScope
    @Bean
    public TestReader reader() {
        return new TestReader();
    }

    @Bean
    public TestProcessor processor() {
        return new TestProcessor();
    }

    /*
    @Bean
    public TestWriter writer() {
        return new TestWriter();
    }
    */
    
    @Bean
    public Step testStep() {
        return stepBuilderFactory.get("testStep").<TestModel, TestModel>chunk(10)
	    .reader(reader())
	    .processor(processor())
	    //.writer(writer())
	    .build();
    }
    
    @Bean
    public Job testLoad() throws Exception {
	System.out.println("Loading...");
        return jobBuilderFactory
	    .get("testLoad")
	    .incrementer(new RunIdIncrementer())
	    .start(testStep())
	    .build();
    }
}
