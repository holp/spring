package edu.msu.simple.configure;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Created by jing on 5/10/17.
 * Modified by Zack on 6/8/2017
 */
@ConfigurationProperties(prefix = "config")
@Data
public class Config {

    private String jobName;
    private String inputFile;
    private String outputFile;
    private List<String> outputFiles;
    
}
