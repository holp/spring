package edu.msu.simple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings("serial")
public class ProSAMWriterException extends RuntimeException {
	private final Logger LOGGER = LoggerFactory.getLogger(getClass().getName());
	private static final int ERROR_CODE = 15;
	
	public ProSAMWriterException(String string, Exception e) {
		super(string, e);
		LOGGER.error("Problem writing out a file for ProSAM. Exiting with RC=" + ERROR_CODE + ": " + string, e);
		System.exit(ERROR_CODE);
	}

}
