package edu.msu.simple.test.model;

//import java.io.Serializable;

//import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.*;

//@SuppressWarnings("serial")
@Data
//public class CorrespondenceModel implements Serializable {
//public class TestModel implements Serializable {
public class TestModel {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(TestModel.class.getName());

    private String value;

}
