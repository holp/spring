package edu.msu.checkfileinfo.repository;

// $Id: FileinfoProdRepository.java,v 1.1 2018-06-11 13:32:47-04 ericholp Exp $

import edu.msu.checkfileinfo.entity.FileinfoProd;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FileinfoProdRepository extends JpaRepository<FileinfoProd, Integer>{
    public FileinfoProd findById(int id);

    List<FileinfoProd> findByDate(String date);
}
