package edu.msu.checkfileinfo.repository;

// $Id: FileinfoQaRepository.java,v 1.1 2018-06-11 13:31:06-04 ericholp Exp $

import edu.msu.checkfileinfo.entity.FileinfoQa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FileinfoQaRepository extends JpaRepository<FileinfoQa, Integer>{
    public FileinfoQa findById(int id);

    List<FileinfoQa> findByDate(String date);
}
