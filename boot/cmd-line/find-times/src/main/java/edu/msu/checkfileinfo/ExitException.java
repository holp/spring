package edu.msu.checkfileinfo;

import org.springframework.boot.ExitCodeGenerator;

public class ExitException extends RuntimeException implements ExitCodeGenerator {

    private int exitcode=0;

    ExitException(){
    }
    
    ExitException(int exitcode){
	this.exitcode=exitcode;
    }
    
    @Override
    public int getExitCode() {
	return exitcode;
    }
}
