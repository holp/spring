package edu.msu.checkfileinfo.repository;

// $Id: FileinfoTestRepository.java,v 1.2 2018-06-11 13:16:46-04 ericholp Exp $

import edu.msu.checkfileinfo.entity.FileinfoTest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FileinfoTestRepository extends JpaRepository<FileinfoTest, Integer>{
    public FileinfoTest findById(int id);

    List<FileinfoTest> findByDate(String date);
}
