package edu.msu.checkfileinfo.entity;

// $Id: FileinfoTest.java,v 1.2 2018-06-11 13:41:49-04 ericholp Exp $

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "fileinfo_test")
public class FileinfoTest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String date;
    private String time;
    private String name;
    private long lastmodifiedint;
    private String lastmodified;
    private long size;
    private String md5;
    private String perms;
    private String owner;
    private String version;
    private boolean accessdenied;
    private String type;

    public void print(){
	System.out.println("'" +
			   id + "' '" +
			   date + "' '" +
			   time + "' '" +
			   name + "' '" +
			   lastmodifiedint + "' '" +
			   lastmodified + "' '" +
			   size + "' '" +
			   md5 + "' '" +
			   perms + "' '" +
			   owner + "' '" +
			   version + "' '" +
			   accessdenied + "' '" +
			   type + "'"
			   );
    }
}
