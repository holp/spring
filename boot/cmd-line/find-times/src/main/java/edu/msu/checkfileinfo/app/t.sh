#!/bin/bash


# ------------------------------------------------------------------------
showdeltat(){
    SECS="$1"

    if [[ ! $SECS =~ ^[0-9]+$ ]]; then
	return 2
    fi
    if (( SECS < 60 )); then
	echo "$SECS secs"
    elif (( SECS < 3600 )); then
	echo "$(( SECS / 60 )) mins, $(( SECS % 60 )) secs ($SECS secs)"	
    else
	local REM=$(( SECS % 3600 ))
	local DISPLAY
	if (( REM < 60 )); then
	    DISPLAY="$REM secs"
	else
	    DISPLAY="$(( REM / 60)) mins, $(( REM % 60 )) secs"
	fi
	
	echo "$(( SECS / 3600 )) hrs, $DISPLAY"
    fi
    return 0
}
# ------------------------------------------------------------------------

F=msu_dis_proc_all_terms19_qa__20180608_170517
F=msu_dis_proc_all_terms19_qa__20180608_100742
F=msu_dis_proc_all_terms19_qa__20180607_153233
F=msu_dis_proc_all_terms19_qa__20180611_

for FILE in $(ls -rt $F*); do
    SPE=$(stat --printf="%Y" $FILE)
    #echo "$SPE $FILE"
    if [[ -n $PREV ]]; then
    	DELTA=$(( SPE - PREV ))
    	DELTA=$(showdeltat $DELTA)
    else
     	DELTA=---
    fi
    # echo "$SPE $PREV $DELTA $FILE"
    printf "%-30s %s\n" "$DELTA" "$FILE"
    echo "$SPE $FILE"
    PREV=$SPE
done


# stat --printf="lastmodify=%Y %y\n" msu_cbac_sync.dat
