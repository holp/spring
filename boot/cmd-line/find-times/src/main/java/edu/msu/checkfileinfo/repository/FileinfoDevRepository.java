package edu.msu.checkfileinfo.repository;

// $Id: FileinfoDevRepository.java,v 1.3 2018-06-11 13:15:13-04 ericholp Exp $

import edu.msu.checkfileinfo.entity.FileinfoDev;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FileinfoDevRepository extends JpaRepository<FileinfoDev, Integer>{
    public FileinfoDev findById(int id);

    List<FileinfoDev> findByDate(String date);
}
