package edu.msu.checkfileinfo.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import edu.msu.checkfileinfo.entity.FileinfoDev;
import edu.msu.checkfileinfo.repository.FileinfoDevRepository;
import edu.msu.checkfileinfo.entity.FileinfoTest;
import edu.msu.checkfileinfo.repository.FileinfoTestRepository;
import edu.msu.checkfileinfo.entity.FileinfoQa;
import edu.msu.checkfileinfo.repository.FileinfoQaRepository;
import edu.msu.checkfileinfo.entity.FileinfoProd;
import edu.msu.checkfileinfo.repository.FileinfoProdRepository;

import java.util.List;

import java.io.File;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.Locale;

//import com.google.common.io.Files;
//import com.google.common.hash.HashCode;
//import com.google.common.hash.Hashing;

//import org.springframework.batch.item.ItemReader;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.io.FilenameFilter;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileOwnerAttributeView;
import java.nio.file.attribute.UserPrincipal;

//import org.springframework.batch.item.ItemReader;
import java.io.FileReader;

//import org.apache.commons.io.LineIterator;
//import org.apache.commons.io.FileUtils;

import java.util.Arrays;

//import org.apache.commons.lang3.tuple.Pair;

//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;

//@Configuration
//@Profile("test")
@Component
public class App {
    @SuppressWarnings("unused")
    private static final Logger log = LoggerFactory.getLogger(App.class.getName());

    @Autowired
    private FileinfoDevRepository devrepo;

    class Pair implements Comparable {
	public long t;
	public File f;
	
	public Pair(File file) {
	    f = file;
	    t = file.lastModified();
	}
	
	public int compareTo(Object o) {
	    long u = ((Pair) o).t;
	    return t < u ? -1 : t == u ? 0 : 1;
	}
    };
    
    
    public void test(){

	FileinfoDev fileinfodev = devrepo.findById(3536);
	if(fileinfodev == null){
	    System.out.println("fileinfo for id=1 is null");
	}else{
	    fileinfodev.print();
	}
	

	if(false){
	    String path="/opt/sigma/qa/rpt/archive/dis/saved";
	    
	    File dir = new File(path);

	    File[] files = dir.listFiles();
	    
	    if(files == null){
		System.out.println("null");
	    }else{
		Pair[] pairs = new Pair[files.length];
		for (int i = 0; i < files.length; i++){
		    pairs[i] = new Pair(files[i]);
		}
		
		Arrays.sort(pairs);
		
		for (int i = 0; i < files.length; i++){
		    files[i] = pairs[i].f;
		}
		
		
		int i = 1;
		long prev=0;
		for (File f : files) {
		    long t = f.lastModified();
		    t=t/1000;
		    if(i == 1){
			//System.out.println(t + " " + f.getName());
			System.out.println("--------- " + f.getName());
		    }else{
			long delta = t-prev;
			//System.out.println(t + " " + f.getName());
			System.out.println(delta + " " + f.getName());
		    }
		    //System.out.println(f.lastModified() + " " + f.getName());
		    //                  1528734091000000
		    //System.out.println("         ^");
		    
		    System.out.println(t + " " + f.getName());
		    prev=t;
		    i++;
		}
	    }

	}
    }
}

