package edu.msu.commandline.entity;

// $Id: Test.java,v 1.3 2018-02-21 15:20:33-04 ericholp Exp $

// test table entity

/*
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | mediumint(9) | NO   | PRI | NULL    | auto_increment |
| field | char(30)     | NO   |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
*/

import javax.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "test")
public class Test {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String field;
}
