package edu.msu.commandline.app;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;

import edu.msu.commandline.entity.Test;
import edu.msu.commandline.repository.TestRepository;

//@Configuration
//@Profile("test")
@Component
public class App {
    //@SuppressWarnings("unused")
    //private static final Logger log = LoggerFactory.getLogger(App.class.getName());

    @Value("${var}")
    private String var;

    //@Autowired
    //public DataSource dataSource;

    @Autowired
    private TestRepository testrepo;

    /*
mysql> select * from test;
+----+--------------+
| id | field        |
+----+--------------+
|  9 | a test       |
| 11 | nibley blip  |
| 14 | kibley snard |
| 15 | an insert    |
| 16 | mooby        |
| 17 | kkkk         |
| 18 | m            |
| 19 | jjjj         |
+----+--------------+
8 rows in set (0.00 sec)
    */


    public void test(){
	System.out.println("var=" + var);
	//System.out.println();
	//System.out.println("datasource=" + dataSource);
	//System.out.println();
	//System.out.println("repo=" + repo);
	Test test = testrepo.findById(14);
	System.out.println("field for id=14 => " + test.getField());
    }
}
